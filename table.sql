USE [takaful_test]
GO

/****** Object:  Table [dbo].[UserProfile]    Script Date: 7/28/2022 1:01:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](150) NULL,
	[Email] [varchar](150) NULL,
	[Password] [varchar](150) NULL,
	[Dob] [date] NULL,
	[Sex] [varchar](50) NULL,
	[Address] [text] NULL,
 CONSTRAINT [PK__UserProf__1788CC4CA18C0EF4] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


