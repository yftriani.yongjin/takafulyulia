﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TakafulTest.Models;

namespace TakafulTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["UserId"] != null)
            {
                using (var db = new DB_Entities())
                {
                    int UserId = (int) Session["UserId"];
                    var data = db.UserProfiles.Where(x => x.UserId == UserId).FirstOrDefault();
                    return View(data);
                }
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public ActionResult Index(int UserId, UserProfile objUser)
        {
            if (ModelState.IsValid)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var data = db.UserProfiles.FirstOrDefault(x => x.UserId == UserId);
                    if (data != null)
                    {
                        data.Name = objUser.Name;
                        data.Email = objUser.Email;
                        data.Dob = objUser.Dob;
                        data.Sex = objUser.Sex;
                        data.Address = objUser.Address;
                        db.SaveChanges();

                        ViewBag.Message = "Data has been updated.";
                        return View();
                    }
                    else
                        return View();
                }
            }
            return View(objUser);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(UserProfile objUser)
        {
            if (ModelState.IsValid)
            {
                using (DB_Entities db = new DB_Entities())
                {
                    var obj = db.UserProfiles.Where(a => a.Email.Equals(objUser.Email) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserId"] = (int) obj.UserId;
                        Session["Name"] = obj.Name.ToString();
                        return RedirectToAction("Index");
                    }
                }
            }
            return View(objUser);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Register()
        {
            ViewBag.Message = "Register Your Account";

            return View();
        }

        [HttpPost]
        public ActionResult Register(UserProfile objUser)
        {
            if (ModelState.IsValid)
            {
                if (objUser.Password == objUser.ConfirmPassword)
                {
                    using (DB_Entities db = new DB_Entities())
                    {
                        db.UserProfiles.Add(objUser);
                        db.SaveChanges();
                        return RedirectToAction("Login");
                    }
                } else
                {
                    ViewBag.Error = "Please confirm password.";
                    return View();
                }
            }
            return View(objUser);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(UserProfile objUser)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (DB_Entities db = new DB_Entities())
                    {
                        var receiverEmail = new MailAddress(objUser.Email.ToString(), "Receiver");
                        string url = "";
                        var obj = db.UserProfiles.Where(a => a.Email.Equals(objUser.Email)).FirstOrDefault();
                        if (obj != null)
                        {
                            url = Url.Action("ChangePassword", "Home",
                            new System.Web.Routing.RouteValueDictionary(new { id = obj.UserId.ToString() }),
                           "https", Request.Url.Host);
                        }
                        var senderEmail = new MailAddress("", "Takaful");
                        var password = "";
                        var sub = "Password reset request";
                        var body = "<h2>Reset your password?</h2><p>If you requested a password reset, click the button below. If you didn't make this request, ignore this email.</p><p><a href=" + url + ">Click Here</a>";
                        var smtp = new SmtpClient
                        {
                            Host = "",
                            Port = 587,
                            EnableSsl = false,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(senderEmail.Address, password)
                        };
                        using (var mess = new MailMessage(senderEmail, receiverEmail)
                        {
                            Subject = sub,
                            Body = body
                        })
                        {
                            mess.IsBodyHtml = true;
                            smtp.Send(mess);
                        }

                        ViewBag.Message = "Link for update password has been sent to your email.";
                        return View();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message.ToString();
            }
            return View(objUser);
        }

        public ActionResult ChangePassword(int? id)
        {
            using (var db = new DB_Entities())
            {
                var data = db.UserProfiles.Where(x => x.UserId == id).FirstOrDefault();
                return View(data);
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(int UserId, UserProfile objUser)
        {
            if (ModelState.IsValid)
            {
                if (objUser.Password == objUser.ConfirmPassword)
                {
                    using (DB_Entities db = new DB_Entities())
                    {
                        var data = db.UserProfiles.FirstOrDefault(x => x.UserId == UserId);
                        if (data != null)
                        {
                            data.Password = objUser.Password;
                            db.SaveChanges();
                            return RedirectToAction("UpdatePasswordSuccess");
                        }
                        else
                            return View();
                    }
                } else
                {
                    ViewBag.Error = "Please confirm password.";
                    return View();
                }
            }
            return View(objUser);
        }

        public ActionResult UpdatePasswordSuccess()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login");
        }
    }
}